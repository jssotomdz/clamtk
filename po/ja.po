# Japanese translation for clamtk
# Copyright (c) Dave M <dave.nerd@gmail>, 2004 - 2020.
# This file is distributed under the same license as the clamtk package.
# Authors: Takahiro Sato, Hajime Mizuno
# Shushi Kurose <md81bird@hitaki.net>
# Ikuya Awashiro <ikuya@fruitsbasket.info>
# Shinichirou Yamada <yamada_strong_yamada_nice_64bit@yahoo.co.jp>
#
msgid ""
msgstr ""
"Project-Id-Version: clamtk\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2020-08-05 05:26-0500\n"
"PO-Revision-Date: 2020-08-14 10:54+0000\n"
"Last-Translator: Shinichirou Yamada "
"<yamada_strong_yamada_nice_64bit@yahoo.co.jp>\n"
"Language-Team: Japanese <ja@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-08-18 07:20+0000\n"
"X-Generator: Launchpad (build d6d0b96812d8def2ca0ffcc25cb4d200f2f30aeb)\n"
"Language: Japanese\n"

#: lib/Analysis.pm:66 lib/GUI.pm:456 lib/GUI.pm:536 lib/GUI.pm:677
#: lib/Results.pm:156 lib/Results.pm:231
msgid "Analysis"
msgstr "解析"

#: lib/Analysis.pm:78 lib/Schedule.pm:66
msgid "ClamTk Virus Scanner"
msgstr "ClamTkウイルススキャナー"

#: lib/Analysis.pm:83
msgid ""
"This section allows you to check the status of a file from Virustotal. If a "
"file has been previously submitted, you will see the results of the scans "
"from dozens of antivirus and security vendors. This allows you to make a "
"more informed decision about the safety of a file.\n"
"\n"
"If the file has never been submitted to Virustotal, you can submit it "
"yourself. When you resubmit the file minutes later, you should see the "
"results.\n"
"\n"
"Please note you should never submit sensitive files to Virustotal.  Any "
"uploaded file can be viewed by Virustotal customers. Do not submit anything "
"with personal information or passwords.\n"
"\n"
"https://www.virustotal.com/gui/home/search"
msgstr ""
"このセクションではVirusTotalでファイルの安全性をチェックできます。ファイルが以前VirusTotalへ提供されている場合、数十のアンチウイルス/"
"セキュリティベンダーによるスキャン結果によってファイルの安全性を確認できます。\n"
"\n"
"ファイルがVirusTotalへ提供されたことがない場合は自分でそのファイルを提供してください。ファイルを送信して数分待つと、結果が表示されるはずです。"
"\n"
"\n"
"注意: "
"VirusTotalの顧客は送信されたファイルを閲覧できるため、個人情報やパスワードなどの機密情報を含むファイルは絶対に送信しないようにしてください。\n"
"\n"
"https://www.virustotal.com/gui/home/search"

#: lib/Analysis.pm:93
msgid "Help"
msgstr "ヘルプ"

#: lib/Analysis.pm:96
msgid "What is this?"
msgstr "これは何?"

#: lib/Analysis.pm:111
msgid "File Analysis"
msgstr "ファイル解析"

#: lib/Analysis.pm:118 lib/Results.pm:36 lib/Results.pm:41
msgid "Results"
msgstr "結果"

#: lib/Analysis.pm:127 lib/History.pm:209 lib/Results.pm:169
#: lib/Schedule.pm:193 lib/Schedule.pm:242
msgid "Close"
msgstr "閉じる"

#: lib/Analysis.pm:184
msgid "Vendor"
msgstr "ベンダー"

#: lib/Analysis.pm:193
msgid "Date"
msgstr "日付"

#: lib/Analysis.pm:202
msgid "Result"
msgstr "結果"

#: lib/Analysis.pm:222
msgid "Save results"
msgstr "結果を保存"

#: lib/Analysis.pm:240
msgid "Check or recheck a file's reputation"
msgstr "ファイルの評価をチェック/再チェックする"

#: lib/Analysis.pm:259 lib/GUI.pm:576 lib/Shortcuts.pm:41
msgid "Select a file"
msgstr "ファイルを選択してください"

#: lib/Analysis.pm:281
msgid "Submit file for analysis"
msgstr "解析するファイルを送信"

#: lib/Analysis.pm:296
msgid "Uploaded files must be smaller than 32MB"
msgstr "アップロードするファイルは32MB以下でなくてはなりません"

#: lib/Analysis.pm:312
msgid ""
"No information exists for this file. Press OK to submit this file for "
"analysis."
msgstr "このファイルに関する情報がありません。解析するには、OKを押してこのファイルを送信してください。"

#: lib/Analysis.pm:323 lib/Analysis.pm:639
msgid "Unable to submit file: try again later"
msgstr "ファイルを送信できません: 後でもう一度試してください"

#: lib/Analysis.pm:336
msgid "View or delete previous results"
msgstr "以前の結果を表示/削除する"

#: lib/Analysis.pm:363
msgid "View file results"
msgstr "ファイル結果を表示"

#: lib/Analysis.pm:428
msgid "Delete file results"
msgstr "ファイル結果を削除"

#: lib/Analysis.pm:501 lib/Analysis.pm:595 lib/Assistant.pm:100 lib/Scan.pm:102
#: lib/Update.pm:154
msgid "Please wait..."
msgstr "お待ちください..."

#: lib/Analysis.pm:637
msgid "File successfully submitted for analysis."
msgstr "解析するファイルの送信が完了しました。"

#: lib/Analysis.pm:697 lib/Analysis.pm:702
msgid "Unable to save file"
msgstr "ファイルを保存できません"

#: lib/Analysis.pm:711
msgid "File has been saved"
msgstr "ファイルを保存しました"

#: lib/App.pm:246
msgid "Unknown"
msgstr "不明"

#: lib/App.pm:397 lib/Prefs.pm:176
msgid "Never"
msgstr "しない"

#: lib/App.pm:455
msgid "Scan for threats..."
msgstr "脅威をスキャンします..."

#: lib/App.pm:456
msgid "Advanced"
msgstr "詳細"

#: lib/App.pm:457 lib/GUI.pm:430 lib/GUI.pm:431 lib/GUI.pm:539
msgid "Scan a file"
msgstr "ファイルをスキャン"

#: lib/App.pm:458 lib/GUI.pm:435 lib/GUI.pm:436 lib/GUI.pm:542
msgid "Scan a directory"
msgstr "フォルダーをスキャン"

#: lib/App.pm:460 lib/GUI.pm:718
msgid "ClamTk is a graphical front-end for Clam Antivirus"
msgstr "ClamTkはClam Antivirusのためのグラフィカルなフロントエンドです"

#: lib/Assistant.pm:38
msgid "Please choose how you will update your antivirus signatures"
msgstr "アンチウイルスのシグネチャのアップデート方法を選択してください"

#: lib/Assistant.pm:54
msgid "My computer automatically receives updates"
msgstr "コンピューターが自動的にアップデートを受信"

#: lib/Assistant.pm:68
msgid "I would like to update signatures myself"
msgstr "自分でシグネチャをアップデート"

#: lib/Assistant.pm:93 lib/Assistant.pm:150 lib/Network.pm:110
msgid "Press Apply to save changes"
msgstr "適用を押して変更点を保存してください"

#: lib/Assistant.pm:122
msgid "Your changes were saved."
msgstr "変更点を保存しました。"

#: lib/Assistant.pm:125 lib/Update.pm:180 lib/Update.pm:186
msgid "Error updating: try again later"
msgstr "アップデート中にエラー: 後でもう一度試してください"

#: lib/GUI.pm:69
msgid "Virus Scanner"
msgstr "ウィルススキャナー"

#: lib/GUI.pm:81 lib/GUI.pm:533 lib/Shortcuts.pm:29
msgid "About"
msgstr "ClamTkについて"

#: lib/GUI.pm:127
msgid "Updates are available"
msgstr "アップデートが利用可能です"

#: lib/GUI.pm:130
msgid "The antivirus signatures are outdated"
msgstr "アンチウイルスのシグネチャが期限切れです"

#: lib/GUI.pm:133
msgid "An update is available"
msgstr "アップデートが利用可能です"

#: lib/GUI.pm:217 lib/GUI.pm:555
msgid "Settings"
msgstr "設定"

#: lib/GUI.pm:218
msgid "View and set your preferences"
msgstr "設定項目を表示したり設定します"

#: lib/GUI.pm:222 lib/GUI.pm:563
msgid "Whitelist"
msgstr "ホワイトリスト"

#: lib/GUI.pm:223
msgid "View or update scanning whitelist"
msgstr "スキャン時のホワイトリストを表示したりアップデートします"

#: lib/GUI.pm:227 lib/GUI.pm:561
msgid "Network"
msgstr "ネットワーク"

#: lib/GUI.pm:228
msgid "Edit proxy settings"
msgstr "プロキシ設定を編集"

#: lib/GUI.pm:232 lib/GUI.pm:545 lib/Schedule.pm:54
msgid "Scheduler"
msgstr "スケジュール"

#: lib/GUI.pm:233
msgid "Schedule a scan or signature update"
msgstr "スキャンまたはシグネチャのアップデートのスケジュールを設定します"

#: lib/GUI.pm:295 lib/GUI.pm:557
msgid "Update"
msgstr "アップデート"

#: lib/GUI.pm:296
msgid "Update antivirus signatures"
msgstr "アンチウイルスのシグネチャのアップデート"

#: lib/GUI.pm:300 lib/GUI.pm:565
msgid "Update Assistant"
msgstr "アップデートアシスタント"

#: lib/GUI.pm:301
msgid "Signature update preferences"
msgstr "シグネチャのアップデートの設定です"

#: lib/GUI.pm:363 lib/GUI.pm:553 lib/GUI.pm:665 lib/History.pm:41
msgid "History"
msgstr "履歴"

#: lib/GUI.pm:364
msgid "View previous scans"
msgstr "以前のスキャンを表示"

#: lib/GUI.pm:368 lib/GUI.pm:559 lib/Results.pm:139 lib/Results.pm:216
msgid "Quarantine"
msgstr "隔離"

#: lib/GUI.pm:369
msgid "Manage quarantined files"
msgstr "隔離ファイルの管理"

#: lib/GUI.pm:457
msgid "View a file's reputation"
msgstr "ファイルの評価を表示"

#: lib/GUI.pm:599 lib/GUI.pm:637 lib/Scan.pm:777 lib/Scan.pm:788
msgid "You do not have permissions to scan that file or directory"
msgstr "そのファイルまたはディレクトリをスキャンする権限がありません"

#: lib/GUI.pm:613 lib/Shortcuts.pm:47 lib/Whitelist.pm:100
msgid "Select a directory"
msgstr "フォルダーを選択してください"

#: lib/GUI.pm:658
msgid "Configuration"
msgstr "設定"

#: lib/GUI.pm:671
msgid "Updates"
msgstr "アップデート"

#: lib/GUI.pm:709
msgid "Homepage"
msgstr "ホームページ"

#: lib/History.pm:80
msgid "View"
msgstr "表示"

#: lib/History.pm:88
msgid "Print"
msgstr "印刷"

#: lib/History.pm:105 lib/Quarantine.pm:98 lib/Results.pm:146
#: lib/Results.pm:224 lib/Schedule.pm:203
msgid "Delete"
msgstr "削除"

#: lib/History.pm:150
#, perl-format
msgid "Viewing %s"
msgstr "%s を表示しています"

#: lib/History.pm:166
#, perl-format
msgid "Problems opening %s..."
msgstr "ファイル %s を開けません..."

#: lib/Network.pm:41
msgid "No proxy"
msgstr "プロキシを使用しない"

#: lib/Network.pm:46
msgid "Environment settings"
msgstr "システムのプロキシ設定を使用する"

#: lib/Network.pm:51
msgid "Set manually"
msgstr "手動でプロキシを設定する"

#: lib/Network.pm:55
msgid "IP address or host"
msgstr "IPアドレスまたはホスト名"

#: lib/Network.pm:76
msgid "Port"
msgstr "ポート番号"

#: lib/Network.pm:263
msgid "Settings saved"
msgstr "設定を保存しました"

#: lib/Network.pm:270
msgid "Error"
msgstr "エラー"

#: lib/Quarantine.pm:56
msgid "Number"
msgstr "番号"

#: lib/Quarantine.pm:67 lib/Results.pm:78
msgid "File"
msgstr "ファイル"

#: lib/Quarantine.pm:85
msgid "Restore"
msgstr "復元"

#: lib/Quarantine.pm:124 lib/Results.pm:281
#, perl-format
msgid "Really delete this file (%s) ?"
msgstr "本当にこのファイルを削除しますか?: %s"

#: lib/Quarantine.pm:185
msgid "Save file as..."
msgstr "名前を付けて保存..."

#: lib/Results.pm:89 lib/Schedule.pm:215
msgid "Status"
msgstr "状態"

#: lib/Results.pm:100
msgid "Action Taken"
msgstr "実行する"

#: lib/Results.pm:219
msgid "Quarantined"
msgstr "隔離済み"

#: lib/Results.pm:227
msgid "Deleted"
msgstr "削除済み"

#: lib/Scan.pm:141
msgid "Preparing..."
msgstr "準備しています..."

#: lib/Scan.pm:152 lib/Scan.pm:415
#, perl-format
msgid "Files scanned: %d"
msgstr "スキャンしたファイル: %d"

#: lib/Scan.pm:156 lib/Scan.pm:486
#, perl-format
msgid "Possible threats: %d"
msgstr "潜在的な脅威: %d"

#: lib/Scan.pm:398
#, perl-format
msgid "Scanning %s..."
msgstr "%s をスキャンしています…"

#: lib/Scan.pm:484
msgid "None"
msgstr "なし"

#: lib/Scan.pm:510
msgid "Cleaning up..."
msgstr "クリーンアップしています..."

#: lib/Scan.pm:517 lib/Update.pm:224 lib/Update.pm:227
msgid "Complete"
msgstr "完了しました"

#: lib/Scan.pm:520
msgid "Scanning complete"
msgstr "スキャンが完了しました"

#: lib/Scan.pm:522
msgid "Possible threats found"
msgstr "潜在的な脅威が見つかりました"

#: lib/Scan.pm:564
msgid "No threats found"
msgstr "脅威となるファイルは見つかりません"

#: lib/Scan.pm:601
#, perl-format
msgid "ClamAV Signatures: %d\n"
msgstr "ClamAVシグネチャ: %d\n"

#: lib/Scan.pm:603
msgid "Directories Scanned:\n"
msgstr "フォルダーのスキャンが完了しました:\n"

#: lib/Scan.pm:608
#, perl-format
msgid ""
"\n"
"Found %d possible %s (%d %s scanned).\n"
"\n"
msgstr ""
"\n"
"%d 個の %s を発見しました（%d %sをスキャン）\n"
"\n"

#: lib/Scan.pm:610
msgid "threat"
msgstr "脅威"

#: lib/Scan.pm:610
msgid "threats"
msgstr "脅威"

#: lib/Scan.pm:612
msgid "file"
msgstr "ファイル"

#: lib/Scan.pm:612
msgid "files"
msgstr "ファイル"

#: lib/Scan.pm:624
msgid "No threats found.\n"
msgstr "脅威は発見されませんでした。\n"

#: lib/Schedule.pm:81
msgid "Scan"
msgstr "スキャン"

#: lib/Schedule.pm:88
msgid "Select a time to scan your home directory"
msgstr "ホームフォルダーをスキャンする時刻を選択"

#: lib/Schedule.pm:92
msgid "Set the scan time using a 24 hour clock"
msgstr "24時間表示でスキャン時刻を設定してください"

#: lib/Schedule.pm:103 lib/Schedule.pm:163
msgid "Set the hour using a 24 hour clock"
msgstr "24時間表示で時間を設定"

#: lib/Schedule.pm:104 lib/Schedule.pm:164
msgid "Hour"
msgstr "時"

#: lib/Schedule.pm:111 lib/Schedule.pm:170
msgid "Minute"
msgstr "分"

#: lib/Schedule.pm:146 lib/Update.pm:80 lib/Update.pm:205 lib/Update.pm:220
msgid "Antivirus signatures"
msgstr "アンチウイルスのシグネチャ"

#: lib/Schedule.pm:153
msgid "Select a time to update your signatures"
msgstr "シグネチャをアップデートする時刻を選択してください"

#: lib/Schedule.pm:224 lib/Schedule.pm:288
msgid "A daily scan is scheduled"
msgstr "毎日のスキャン実行のスケジュールあり"

#: lib/Schedule.pm:229 lib/Schedule.pm:301
msgid "A daily definitions update is scheduled"
msgstr "毎日の定義アップデートのスケジュールあり"

#: lib/Schedule.pm:292
msgid "A daily scan is not scheduled"
msgstr "毎日のスキャン実行のスケジュールなし"

#: lib/Schedule.pm:306
msgid "A daily definitions update is not scheduled"
msgstr "毎日の定義アップデートのスケジュールなし"

#: lib/Schedule.pm:315
msgid "You are set to automatically receive updates"
msgstr "自動的にアップデートを受信する設定です"

#: lib/Settings.pm:42
msgid "Scan for PUAs"
msgstr "不要なアプリケーション(PUA)をスキャンする"

#: lib/Settings.pm:44
msgid "Detect packed binaries, password recovery tools, and more"
msgstr "プログラム集やパスワードリカバリツールなどを検出します"

#: lib/Settings.pm:57
msgid "Use heuristic scanning"
msgstr "ヒューリスティックスキャンを使用する"

#: lib/Settings.pm:72
msgid "Scan files beginning with a dot (.*)"
msgstr "ドットで始まるファイル (.*) をスキャンする"

#: lib/Settings.pm:73
msgid "Scan files typically hidden from view"
msgstr "通常スキャンファイルは表示から隠されています"

#: lib/Settings.pm:86
msgid "Scan files larger than 20 MB"
msgstr "20MB以上のファイルもスキャンする"

#: lib/Settings.pm:88
msgid "Scan large files which are typically not examined"
msgstr "通常は検査されない巨大ファイルをスキャンします"

#: lib/Settings.pm:101
msgid "Scan directories recursively"
msgstr "ディレクトリを再帰的にスキャンする"

#: lib/Settings.pm:103
msgid "Scan all files and directories within a directory"
msgstr "フォルダーにあるすべてのファイルとフォルダーをスキャンします"

#: lib/Settings.pm:116
msgid "Check for updates to this program"
msgstr "このプログラムのアップデートをチェックする"

#: lib/Settings.pm:118
msgid "Check online for application and signature updates"
msgstr "アプリケーションとシグネチャのアップデートをオンラインでチェックする"

#: lib/Shortcuts.pm:35
msgid "Exit"
msgstr "終了"

#: lib/Update.pm:60
msgid "Product"
msgstr "製品"

#: lib/Update.pm:68
msgid "Installed"
msgstr "インストール済"

#: lib/Update.pm:105
msgid "You are configured to automatically receive updates"
msgstr "アップデートを自動取得するように設定されています"

#: lib/Update.pm:108
msgid "Check for updates"
msgstr "アップデートの確認"

#: lib/Update.pm:199
msgid "Downloading..."
msgstr "ダウンロードしています..."

#: lib/Whitelist.pm:48
msgid "Directory"
msgstr "フォルダー"

#: lib/Whitelist.pm:70
msgid "Add a directory"
msgstr "フォルダーを追加"

#: lib/Whitelist.pm:83
msgid "Remove a directory"
msgstr "フォルダーを削除"
