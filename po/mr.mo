��             +         �     �     �     �     �     �          	       )   +     U     Z     b     j  O   p     �     �     �     �     �     �               "     +     D  &   X          �     �     �     �  �  �  "   �     �     �  ?   �  8        E  %   _  :   �  p   �     1     ?     R     h  �   �  G   D	  "   �	     �	  .   �	     �	     
  >   0
     o
     �
  E   �
  C   �
  {   /     �     �  V   �  ;   (     d                                                                	                                                                  
              About Advanced Date Delete file results Error opening file Exit File Analysis File has been saved File successfully submitted for analysis. Help History Network Never No information exists for this file. Press OK to submit this file for analysis. No information on this file Please wait... Quit Restore Result Results Save results Select a file Settings Submit file for analysis Unable to save file Unable to submit file: try again later Unknown View View or delete previous results Your changes were saved. file Project-Id-Version: clamtk
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-01-10 07:13-0600
PO-Revision-Date: 2014-01-17 04:49+0000
Last-Translator: प्रसाद पाटील <p.prasadpatil@gmail.com>
Language-Team: Marathi <mr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-01-17 22:44+0000
X-Generator: Launchpad (build 16901)
 आमच्या विषयी प्रगत दिनांक फाईलचे निकाल काढून टाका फाईल उघडण्यात त्रुटी बाहेर पडा फाईल विश्लेषण फाइल जतन केली गेली आहे फाईल विश्लेषणासाठी यशस्वीरित्या जमा केली मदत(_H) इतिहास नेटवर्क कधीच नाही या फाईल संबधीची माहिती आस्तित्वात नाही. या फाईलच्या विश्लेषणासाठी OK दाबा या फाईल संबंधी माहिती नाही. कृपया थांबा... बंद करा पूर्वस्थितीत आणा गुणपत्रिका गुणपत्रिका गुणपत्रिका साठवून ठेवा फाइल निवडा सेटिंग्ज फाईल विश्लेषणसाठी जमा करा फाईल साठवून ठेवू शकत नाही फाईल जमा करू शकत नाही: थोडया वेळाने प्रयत्न करा अपरिचित दृष्य मागील निकाल पहा किंवा काढून टाका आपले बदल जतन केले गेले. फाइल 